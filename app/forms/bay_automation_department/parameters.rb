module BayAutomationDepartment
  class Parameters < BayForm::Base

    ATTRIBUTES = [ :classname, :color, :title, :breadcrumbs ]
    attr_accessor *ATTRIBUTES

    validates_presence_of :title
    validates_presence_of :color

    def human_readable
      "create department tag with name #{title} and color #{color} class #{classname} and breadcrumbs #{breadcrumbs}"
    end

  end
end