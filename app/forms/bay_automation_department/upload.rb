module BayAutomationDepartment
  class Upload < BayForm::Base

    ATTRIBUTES = [ :file, :parameters ]
    attr_accessor *ATTRIBUTES

    validates_presence_of :file, :parameters

    def doc
      @doc ||= Nokogiri::HTML(self.file.read)
    end

    def parameter_object
      @parameter_object ||= Parameters.new(self.parameters)
    end

    def process
      return BayAutomationDepartment.process(doc, self.parameters )
    end

  end
end