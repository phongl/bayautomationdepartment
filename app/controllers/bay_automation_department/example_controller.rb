require_dependency "bay_automation_department/application_controller"

module BayAutomationDepartment
  class ExampleController < ApplicationController

    # GET
    def index
      @upload_form = Upload.new
      render :new
    end

    # GET
    def new
      @upload_form = Upload.new
    end

    # POST
    def create
      @upload_form = Upload.new(upload_params)
      respond_to do |format|
        if @upload_form.submit
          doc = @upload_form.process
          @str = doc.to_html
          format.html { render :layout => false }
        else
          format.html { render :new }
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def upload_params
      properties_keys = params[:upload].try(:fetch, :parameters, {}).keys
      params.require(:upload).permit(:file, parameters: properties_keys)
    end

  end
end
