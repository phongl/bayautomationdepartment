module BayAutomationDepartment
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception

    helper BayBootstrapHelper
  end
end
