$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "bay_automation_department/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "bay_automation_department"
  s.version     = BayAutomationDepartment::VERSION
  s.authors     = ["Phong Le"]
  s.email       = ["phong.le@bayphoto.com"]
  s.homepage    = "http://www.bayphoto.com"
  s.summary     = "Summary of BayAutomationDepartment."
  s.description = "Description of BayAutomationDepartment."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails"
  s.add_dependency "nokogiri"
  s.add_dependency "bay_form"
  s.add_dependency "bay_bootstrap_helper"

  s.add_development_dependency "sqlite3"
end
