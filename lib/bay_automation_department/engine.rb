module BayAutomationDepartment
  class Engine < ::Rails::Engine
    isolate_namespace BayAutomationDepartment
    require 'bay_bootstrap_helper'
    require 'bay_form'
  end
end
