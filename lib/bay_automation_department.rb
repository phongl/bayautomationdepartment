require "bay_automation_department/engine"
require "nokogiri"

module BayAutomationDepartment

  class Core
    def initialize(doc, options={})
      @doc = doc
      @color = options[:color]
      @classname = options[:classname]
      @department = options[:title]
      @breadcrumbs = options[:breadcrumbs] || ""
    end

    def target_node
      @target_node ||= @doc.at('//*[@id="departments"]')
    end

    def new_node
      @new_node ||= begin
        node = Nokogiri::XML::Node.new('div',@doc)
        node['id'] = @department.downcase.parameterize
        node['class'] = @classname
        node['style'] = "border-top-color: #{@color}; border-left-color: #{@color}"
        ants = @breadcrumbs.split(',').map{|word| "<span class='department_ant'>#{word}________</span>"}.join('')
        node.inner_html = "<table><tr><td class='department_date'></td><td class='department_breadcrumbs'><span class='department_title' style='color:#{@color}'>#{@department}</span>#{ants}</td><td class='department_qc'></td></tr></table>"
        # node.inner_html = "<div class='department_date'></div><div class='department_breadcrumbs'><span class='department_title'>#{@department}</span>#{ants}</div><div class='department_qc'></div>"
        node
      end
    end

    def process
      new_node.parent = target_node

      return @doc
    end

  end

  def self.process( doc,  options = {} )
    obj = BayAutomationDepartment::Core.new(doc, options)
    return obj.process
  end

end
